CXX = clang++
CPPFLAGS = -Wall
LIB = -lsfml-system -lsfml-window -lsfml-graphics
SRC = $(wildcard *.cpp)
OBJ = $(patsubst %.cpp, %.o, $(SRC))
PROGRAM = game

$(PROGRAM): $(OBJ)
	$(CXX) $(CPPFLAGS) $^ -o $@ $(LIB)  

%.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(PROGRAM)
